#ifdef MATLAB
#include "mex.h"
#endif

#include "parse_func.h"
#include "Symbol_Table.h"

// Global variables defined for the parser utility functions
int nRes;
int nCap;
int nInd;
int nVsrc;
int nIsrc;
int nVCCS;
int nVCVS;
int nCCCS;
int nCCVS;

void Init_parse_util()
{
	nRes = 0;
	nCap = 0;
	nInd = 0;
	nVsrc = 0;
	nIsrc = 0;
	nVCCS = 0;
}


void ParseRes(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 0;
	//char elem_name[5];

	Node_Entry **nodelist;
	
	printf("RES[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);

	nRes++;

	nodelist = malloc(sizeof(Node_Entry*) * numnodes);

	nodelist[0] = Insert_Node_Entry(node1);
	nodelist[1] = Insert_Node_Entry(node2);

	Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseCap(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 1;
	Node_Entry **nodelist;
	
	//printf("[Capacitor....]\n");
	printf("CAP[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);
	//printf("   id_elem=%s, no+=%s, no-=%s, C=%e\n", name, node1, node2, value);
	nCap++;

	 // Get the nodes
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseInd(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 2;
	Node_Entry **nodelist;
	
	//printf("[Indutor....]\n");
	//printf("   id_elem=%s, no+=%s, no-=%s, L=%e\n", name, node1, node2, value);
	printf("IND[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);
	nInd++;

	 // Get the nodes
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseVsrc(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 3;
	Node_Entry **nodelist;
	
	//printf("[Fonte de Tensao....]\n");
	//printf("   id_elem=%s, no+=%s, no-=%s, V=%e\n", name, node1, node2, value);
	printf("VSRC[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);
	nVsrc++;

	 // Get the nodes
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseIsrc(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 4;
	Node_Entry **nodelist;
	
	printf("ISRC[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);
	nIsrc++;

	// Save the device, nodes, value info to the symbol tables.
	// Please write your own code here ...
	 // Get the nodes
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseVCCS(char *name, char *node1, char *node2, char *node3, char *node4, double value)
{
	int numnodes = 4;
	int tipo = 5;
	Node_Entry **nodelist;
	
	//printf("[VCCS....]\n");
	//printf("   name=%s, N+=%s, Ne-=%s,  Nc+=%s, Nc-=%s, G=%e\n", 
//			name, node1, node2, node3, node4, value);
	printf("VCCS[%s];\t n+[%s];\t Ne-[%s];\t Nc+[%s];\t Nc-[%s];\t valor[%e]\n", name, node1, node2, node3, node4, value);

	nVCCS++;

	 // Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    nodelist[3] = Insert_Node_Entry(node4);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseVCVS(char *name, char *node1, char *node2, char *node3, char *node4, double value)
{
	int numnodes = 4;
	int tipo = 6;
	Node_Entry **nodelist;
	
	//printf("[VCCS....]\n");
	//printf("   name=%s, N+=%s, Ne-=%s,  Nc+=%s, Nc-=%s, G=%e\n", 
//			name, node1, node2, node3, node4, value);
	printf("VCVS[%s];\t n+[%s];\t Ne-[%s];\t Nc+[%s];\t Nc-[%s];\t valor[%e]\n", name, node1, node2, node3, node4, value);

	nVCVS++;

	 // Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    nodelist[3] = Insert_Node_Entry(node4);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseCCCS(char *name, char *node1, char *node2, char *node3, char *node4, double value)
{
	int numnodes = 4;
	int tipo = 7;
	Node_Entry **nodelist;
	
	printf("CCCS[%s];\t n+[%s];\t Ne-[%s];\t Nc+[%s];\t Nc-[%s];\t valor[%e]\n", name, node1, node2, node3, node4, value);

	nCCCS++;

	 // Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    nodelist[3] = Insert_Node_Entry(node4);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseCCVS(char *name, char *node1, char *node2, char *node3, char *node4, double value)
{
	int numnodes = 4;
	int tipo = 8;
	Node_Entry **nodelist;
	
	printf("CCVS[%s];\t n+[%s];\t Ne-[%s];\t Nc+[%s];\t Nc-[%s];\t valor[%e]\n", name, node1, node2, node3, node4, value);

	nCCVS++;

	 // Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    nodelist[3] = Insert_Node_Entry(node4);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseDiodo(char *name, char *node1, char *node2, double value)
{
	int numnodes = 2;
	int tipo = 9;
	Node_Entry **nodelist;
	
	printf("DIODO[%s];\t n+[%s];\t n-[%s];\t valor[%e]\n", name, node1, node2, value);
	nCap++;

	 // Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseTjb(char *name, char *node1, char *node2, char *node3, double value)
{
	int numnodes = 3;
	int tipo = 10;
	Node_Entry **nodelist;
	printf("TJB[%s];\t nE[%s];\t nC[%s];\t nB[%s];\t valor[%e]\n", name, node1, node2, node3, value);
	nCap++;

	 // Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void ParseMosfet(char *name, char *node1, char *node2, char *node3, double value)
{
	int numnodes = 3;
	int tipo = 11;
	Node_Entry **nodelist;

	printf("Mosfet[%s];\t nE[%s];\t nC[%s];\t nB[%s];\t valor[%e]\n", name, node1, node2, node3, value);
	nCap++;

	 // Nodos
    nodelist = malloc(sizeof(Node_Entry*) * numnodes);

    nodelist[0] = Insert_Node_Entry(node1);
    nodelist[1] = Insert_Node_Entry(node2);
    nodelist[2] = Insert_Node_Entry(node3);
    
    Insert_Device_Entry(tipo, name, numnodes, nodelist, value);
}

void Summarize()
{
	printf("\n\nTotal:\n   #res=%d, #cap=%d, #ind=%d, #vccs=%d, #vsrc=%d, #isrc=%d\n", \
		nRes, nCap, nInd, nVCCS, nVsrc, nIsrc);
	printf("\n\nFim..............]\n");


	printf("Imprime a device table...");
	Print_Device_Table();

}


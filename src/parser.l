/*
  Renan Bortoluzzi
*/
%{
#include <stdio.h>
#include <stdlib.h>
#include "parser.h"  // arquivo gerado pelo bison

int lines = 1;

int* returnLine(int line);

%}

%x COMMENT

DIGIT [0-9]

INT              (\-?){DIGIT}+
DOUBLE           (\-?){DIGIT}*\.{DIGIT}+
WHITESPACE       [ \t]
CHAR [A-Z]
char [a-z]

/*Unidades de medida*/
FEMTO f
PICO  p
NANO  n
MICRO u
MILLI m
KILO  k
MEGA  meg
GIGA  g
TERA  t

UNIT ({MILLI}|{MICRO}|{NANO}|{PICO}|{FEMTO}|{KILO}|{MEGA}|{GIGA}|{TERA})

VALUE ({DOUBLE}|[-]?{IN}){UNIT}

R     R
L     L
C     C
V     V
I     I

/*Element ID*/
EID   (({CHAR}|{char}|{DIGIT})+)

//Elements
RESISTOR 		[R]{ALPHANUM}+
CAPACITOR		[C]{ALPHANUM}+
INDUCTOR		[L]{ALPHANUM}+
VSRC			[V]{ALPHANUM}+
CSRC			[I]{ALPHANUM}+
VCVS			[E]{ALPHANUM}+
CCCS			[F]{ALPHANUM}+
VCCS			[G]{ALPHANUM}+
CCVS			[H]{ALPHANUM}+
DIDO			[D]{ALPHANUM}+
TJB			[Q]{ALPHANUM}+
MOSFET			[M]{ALPHANUM}+

%%

[ \t]+ { }
\n      { lines++; }
\/\/.*\n  { lines++; }


{VSRC}{
   yylval.sv = new char[yyleng] ;
   strcpy(yylval.sv , yytext) ;
   BEGIN INT;
   return T_VSRC;
}

{ISRC}{
   yylval.sv = new char[yyleng] ;
   strcpy(yylval.sv , yytext) ;
   BEGIN INT ;
   return T_ISRC;
}

{RESISTOR}{
   yylval.sv = new char[yyleng] ;
   strcpy(yylval.sv , yytext) ;
   BEGIN INT;
   return T_RID;
}

{CAPACITOR}{
   yylval.sv = new char[yyleng] ;
   strcpy(yylval.sv , yytext) ;
   BEGIN INT;
   return T_CID;
}

{INDUTOR}{
   yylval.sv = new char[yyleng] ;
   strcpy(yylval.sv , yytext) ;
   BEGIN INT;
   return T_LID;
}
		
"/*" BEGIN(COMMENT);
<COMMENT>{
     "*/"      BEGIN(INITIAL);
     [^*\n]+   
     "*"       
     \n        lines++;
}

.       return TOKEN_ERRO;

%%

int* returnLine(int line){
  int* pLine;
  pLine = (int *)malloc(sizeof(int));
  *pLine = line;
  return pLine;
}
